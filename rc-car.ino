#include "timer.h"
#include "wifi.h"
#include "ota.h"
#include "web-server.h"
#include "rc-vehicle.h"

std::unique_ptr<WiFiNetwork> wifiNetwork;
std::unique_ptr<OTA> ota;
std::unique_ptr<WebServer> webServer;
std::unique_ptr<Vehicle> vehicle;

/* Repeat operation every minute */
unsigned long previousMillis = 0; // Last time trigger
int interval = 60 * 1000; // Interval (repeat)

void setup() {
  Serial.begin(115200); // Start the Serial communication to send messages to the computer
  delay(10);
  Serial.println("Serial ready");

  String deviceHostname = "ESP8266";
  String devicePassword = "esp8266";

  // Connect to WiFi
  wifiNetwork.reset(new WiFiNetwork());

  // set-up OTA updates
  ota.reset(new OTA(deviceHostname, devicePassword));
  
  // Create new web-server instance
  webServer.reset(new WebServer(80));

  // Create new vehicle instance
  vehicle.reset(new Vehicle(&webServer->server));

  previousMillis = millis(); // Delay next action trigger by a defined interval time once all is set-up
}

void loop() {
  bool triggerOneMinuteAction = false;
  if(scheduleRepeatAction(&previousMillis, interval)) {
    triggerOneMinuteAction = true;
    //ASCII for a new page ESC + [2J
    //Serial.println("\x1b[2J");
    Serial.println(u8"\u0012b[2J");
  }
	
  wifiNetwork->continuousLoop(triggerOneMinuteAction);
  ota->continuousLoop(triggerOneMinuteAction);
  webServer->continuousLoop(triggerOneMinuteAction);
}
