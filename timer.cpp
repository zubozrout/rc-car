#include "timer.h"

/*
 * Schedule repeat action by calling this function in setup,
 * passing reference to last call previousMillis and interval in milliseconds
 */

bool scheduleRepeatAction(unsigned long *previousMillis, int interval) {
	if(millis() - *previousMillis >= interval) {
		*previousMillis = millis();   
		return true;
	}
	return false;
}
