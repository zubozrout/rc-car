#include "wifi.h"

WiFiNetwork::WiFiNetwork(bool enableDNS, String deviceHostname) {
  this->deviceHostname = deviceHostname;
  serialPrint("Initializing WiFiNetwork");
  setupWiFi(enableDNS);
}

void WiFiNetwork::setupWiFi(bool enableDNS) {
  WiFi.hostname(deviceHostname.c_str());

  // start up WiFi manager with local network hostname
  serialPrint("Connecting ...");
  wifiManager.setConfigPortalTimeout(20);
  if(!wifiManager.autoConnect("ESP8266", "deadbeef11")) {
    serialPrint("Keeping in AP mode");
  }
  else {
    serialPrint("Connnected to a local WiFi network");
  }

  if(enableDNS) {
    if(MDNS.begin(deviceHostname.c_str())) { // Start the mDNS responder for esp8266.local
      serialPrint("mDNS responder started");
    }
    else {
      serialPrint("Error setting up MDNS responder!");
    }
  }
  
  serialPrintStatus();
}

void WiFiNetwork::serialPrint(String message) {
  Serial.println("[WiFiNetwork] " + message);
}

void WiFiNetwork::serialPrintStatus() {
  serialPrint(String("Connected to:\t") + WiFi.SSID()); // Tell us what network we're connected to
  serialPrint(String("IP address:\t") + WiFi.localIP().toString()); // Tell us our assigned IP address
}

void WiFiNetwork::continuousLoop(bool triggerOneMinuteAction) {
  if(triggerOneMinuteAction) {
    serialPrintStatus();
  }
}
