#include "rc-vehicle.h"

Vehicle::Vehicle(ESP8266WebServer *server) {
  serialPrint(String("Starting up vehicle reachable on default path: ") + webPath + String(" ..."));
  this->server = server;
  init();
}

Vehicle::Vehicle(ESP8266WebServer *server, String webPath) {
  serialPrint(String("Starting up vehicle reachable on custom path: ") + webPath + String(" ..."));
  this->server = server;
  this->webPath = webPath;
  init();
}

void Vehicle::init() {
  // Define GPIO's defaults
  pins.emplace_back(0, true, "red lights", "red-lights"); // Brakes
  pins.emplace_back(2, true, "front lights", "front-lights"); // Lights
  pins.emplace_back(15, true, "lights", "inside-lights"); // Lights
  pins.emplace_back(13, true, "lights", "inside-lights"); // Lights
  pins.emplace_back(12, true, "motor speed", "motor-speed"); // Light + Motor
  pins.emplace_back(14, true, "motor direction", "motor-direction"); // Motor
  pins.emplace_back(16, true, "lights", "winkers"); // May not work? Lights
  
  // Setup GPIOs
  for(const auto & pin : pins) {
    pinMode(pin.id, OUTPUT);
  }

  setPostAction();
}

void Vehicle::serialPrint(String message) {
  Serial.println("[Vehicle] " + message);
}
  
void Vehicle::setPostAction() {
  server->on("/control", HTTP_POST, [this]() {
    String action = server->arg("action");
    String setState = server->arg("new-state");
    makeAction(action ? action : "", setState != nullptr ? setState.toInt() : 0);
    
    server->send(200, "application/json", getStatusInfo());
    /*
    String message = String("\n") + server->uri() + String("\n");
    for(int i = 0; i < server->args(); i++) {
      message += "Arg nº" + (String)i + " –> ";
      message += server->argName(i) + ": ";
      message += server->arg(i) + "\n";
    }
    serialPrint(message);
    */
  });
}

String Vehicle::getStatusInfo() {
  String JSON = "{\"pins\": [\n";
  for(auto it = pins.begin(); it != pins.end(); ++it) {
    int currentValue = 0;
    if(it->digital) {
      currentValue = digitalRead(it->id);
    }
    else {
      currentValue = analogRead(it->id);
    }

    JSON += "\t{\n";
    JSON += String("\t\t\"id\": ") + it->id + ",\n";
    JSON += String("\t\t\"digital\": ") + (it->digital ? "true" : "false") + ",\n";
    JSON += String("\t\t\"pinName\": \"") + it->pinName + "\",\n";
    JSON += String("\t\t\"actionName\": \"") + it->actionName + "\",\n";
    JSON += String("\t\t\"currentValue\": ") + currentValue + "\n";
    if(std::next(it) == pins.end()) {
      JSON += "\t}\n";
    }
    else {
      JSON += "\t},\n";
    }
  }
  JSON += "]}\n";
  return JSON;
}
  
void Vehicle::makeAction(String action, int stateValue) {
  if(action == "all-lights") {
    frontLights(stateValue > 0 ? true : false);
    redLights(stateValue > 0 ? true : false);
    return;
  }
  if(action == "front-lights") {
    frontLights(stateValue > 0 ? true : false);
    return;
  }
  if(action == "red-lights") {
    redLights(stateValue > 0 ? true : false);
    return;
  }
  if(action == "motor-speed") {
    runMotorAtSpeed(stateValue);
    return;
  }
  if(action == "motor-direction") {
    runMotorDirection(stateValue > 0 ? true : false);
    return;
  }
  // Some default action?

  serialPrint("Action: " + String(action) + ", " + stateValue);
}

int Vehicle::getPinIdByName(String pinName) {
  for(const auto & pin : pins) {
    if(pin.actionName == pinName) {
      serialPrint("getPinIdByName: " + String(pinName) + ", " + pin.id);
      return(pin.id);
    }
  }
  return -1;
}

void Vehicle::frontLights(bool turnOn) {
    digitalWrite(getPinIdByName("front-lights"), turnOn ? HIGH : LOW);
    serialPrint("frontLights set to: " + String(turnOn ? "HIGH" : "LOW"));
  }

void Vehicle::redLights(bool turnOn) {
  digitalWrite(getPinIdByName("red-lights"), turnOn ? HIGH : LOW);
  serialPrint("redLights set to: " + String(turnOn ? "HIGH" : "LOW"));
}

// Speed can be any value in between 0 and 255
void Vehicle::runMotorAtSpeed(int speed) {
  analogWrite(getPinIdByName("motor-speed"), speed >= 0 && speed <= 255 ? speed : 0);
  serialPrint("runMotorAtSpeed set to: " + speed);
}

void Vehicle::runMotorDirection(bool direction) {
  analogWrite(getPinIdByName("motor-direction"), direction ? HIGH : LOW);
  serialPrint("runMotorDirection set to: " + direction);
}

int Vehicle::getMotorSpeed() {
  int pinId = getPinIdByName("motor-speed");
  return pinId >= 0 ? analogRead(pinId) : -1; // Returns int(0 to 255)
}

void Vehicle::continuousLoop(bool triggerOneMinuteAction) {
  if(triggerOneMinuteAction) {
    serialPrint("powered up");
  }
}
