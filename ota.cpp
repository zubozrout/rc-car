#include "ota.h"

OTA::OTA(String deviceHostname, String devicePassword) {
  serialPrint("Initializing Arduino OTA");
  this->deviceHostname = deviceHostname;
  this->devicePassword = devicePassword;
  arduinoOtaSetup();
}

void OTA::arduinoOtaSetup() {
  ArduinoOTA.setHostname(deviceHostname.c_str());
  if(devicePassword.length() > 0) {
    ArduinoOTA.setPassword(devicePassword.c_str());
  }
  
  ArduinoOTA.onStart([]() {
    Serial.println("OTA start");
  });
  
  ArduinoOTA.onEnd([]() {
    Serial.println("\nOTA end");
  });
  
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if(error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if(error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if(error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if(error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if(error == OTA_END_ERROR) Serial.println("End Failed");
  });
  
  ArduinoOTA.begin();
  serialPrint("is ready");
}

void OTA::serialPrint(String message) {
  Serial.println("[OTA] " + message);
}

void OTA::serialPrintStatus() {
  serialPrint("OTA hostname:\t" + deviceHostname); // Tell us hostname
  serialPrint("OTA password:\t" + devicePassword); // Tell us password
}

void OTA::continuousLoop(bool triggerOneMinuteAction) {
  ArduinoOTA.handle();
  if(triggerOneMinuteAction) {
    serialPrintStatus();
  }
}
