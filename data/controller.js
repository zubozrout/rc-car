"use strict";

class VehicleController {
	constructor(data) {
		data = data || {};
		this.serverURL = data.url || window.location.href;
		if(this.serverURL.match(/file:\/\/\//)) {
			this.serverURL = "http://192.168.4.1/control";
		}
		this.method = data.method || "POST";
		document.addEventListener("DOMContentLoaded", (event) => {
			this.controls = data.controls || document.querySelectorAll(".controls button");
			this.init();
		});
	}
	
	commonRequest(formData) {
		return new Promise((resolve, reject) => {
			const xhr = new XMLHttpRequest();
			xhr.open(this.method, this.serverURL);
			if(this.method === "POST") {
				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			}
			xhr.onload = () => resolve(xhr.responseText);
			xhr.onerror = () => reject(xhr.statusText);
			xhr.send(formData ? formData : null);
		});
	}
	
	getVehicleState() {
		this.commonRequest().then((response) => {
			console.log(response);
			try {
				let currentState = JSON.parse(response);
				console.log(currentState);
				this.reflectStateInUI(currentState);
			} catch (error) {
				console.error("Controller/Server error, can't parse the given JSON file", error);
			}
		}).catch((error) => {
			console.error("Controller/Server error", error);
		});
	}
	
	init() {
		for(let i = 0; i < this.controls; i++) {
			let button = this.controls[i];
			button.addEventListener("click", (event) => {
				this.getVehicleState();
				//event.currentTarget
			});
		}
		
		this.getVehicleState();
	}
	
	getUIButtons() {
		if(!this.buttons) {
			let controlSection = document.querySelector(".controls");
			this.buttons = controlSection.querySelectorAll(".button");
		}
		return this.buttons;
	}
	
	setButtonState(button, state) {
		button.dataset.state = state;
		let stateValue = state === "off" ? 0 : 1;
		if(button.classList.contains("analog") && stateValue > 0) {
			stateValue = 255;
		}
		return stateValue;
	}
	
	reflectStateInUI(state) {
		let buttons = this.getUIButtons();
		for(let i = 0; i < buttons.length; i++) {
			let pins = state.pins;
			for(let key in pins) {
				if(pins[key].actionName === buttons[i].id) {
					this.setButtonState(buttons[i], pins[key].currentValue > 0 ? "on" : "off");
					break;
				}
			}
		}
	}
	
	mapActions() {
		let buttons = this.getUIButtons();
		for(let i = 0; i < buttons.length; i++) {
			let button = buttons[i];
			this.setButtonState(button, "off");
			
			button.addEventListener("click", (event) => {
				let stateValue = this.setButtonState(event.target, event.target.dataset.state === "on" ? "off" : "on");
				
				let queryString = new URLSearchParams();
				queryString.append("action", event.target.id);
				queryString.append("new-state", stateValue);
				
				this.commonRequest(queryString).then((response) => {
					let currentState = JSON.parse(response);
					console.log(currentState);
					this.reflectStateInUI(currentState);
				});
			});
		}
	}
}

let vehicleController = new VehicleController();
document.addEventListener("DOMContentLoaded", (event) => {
	vehicleController.mapActions();
});
