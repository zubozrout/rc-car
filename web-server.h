#ifndef WEB-SERVER_H
#define WEB-SERVER_H

#include <ESP8266WebServer.h>
#include <FS.h> // Include the SPIFFS library

class WebServer {
  private:
    void init();
    void serialPrint(String message);
    String getContentType(String filename);
    bool handleFileRead(String path);
  
  public:
    ESP8266WebServer server;
    WebServer(int port = 80);
    void continuousLoop(bool triggerOneMinuteAction = false);
};

#endif
