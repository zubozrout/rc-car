#include "web-server.h"

WebServer::WebServer(int port): server(port) {
  serialPrint(String("Initializing web server on port: ") + port);
  init();
}

void WebServer::init() {
  SPIFFS.begin();

  // If the client requests any URL
  server.onNotFound([this]() {
    if(!handleFileRead(server.uri())) {
      // display error 404 (Not Found) if the url is not found
      server.send(404, "text/plain", "404: Not Found");
    }
  });

  server.begin();
  serialPrint("Server started");
}

void WebServer::serialPrint(String message) {
  Serial.println("[WebServer] " + message);
}

String WebServer::getContentType(String filename) {
  if(filename.endsWith(".htm")) return "text/html";
  else if(filename.endsWith(".html")) return "text/html";
  else if(filename.endsWith(".css")) return "text/css";
  else if(filename.endsWith(".js")) return "application/javascript";
  else if(filename.endsWith(".svg")) return "image/svg+xml";
  else if(filename.endsWith(".png")) return "image/png";
  else if(filename.endsWith(".gif")) return "image/gif";
  else if(filename.endsWith(".jpg")) return "image/jpeg";
  else if(filename.endsWith(".ico")) return "image/x-icon";
  else if(filename.endsWith(".xml")) return "text/xml";
  else if(filename.endsWith(".pdf")) return "application/x-pdf";
  else if(filename.endsWith(".zip")) return "application/x-zip";
  else if(filename.endsWith(".gz")) return "application/x-gzip";
  return "text/plain";
}

// Send the right file to the client (if it exists) and return true/false appropriately
bool WebServer::handleFileRead(String path) {
  serialPrint("handleFileRead: " + path);
  
  // If a folder is requested, send the index file (SPIFFS is a flat filesystem, no folders available!)
  if(path.endsWith("/")) {
    path += "index.html";
  }
  String pathWithGz = path + ".gz"; // Potential compressed file path
  
  // Get the MIME type
  String contentType = getContentType(path);

  bool notCompressedExists = SPIFFS.exists(path);
  bool compressedExists = SPIFFS.exists(pathWithGz);
  // If the file exists, either as a compressed archive, or normal
  if(notCompressedExists || compressedExists) {
    // If there's a compressed version available
    if(compressedExists) {                      
      path = pathWithGz; // Use the compressed version
    }
    
    File file = SPIFFS.open(path, "r"); // Open the file
    size_t sent = server.streamFile(file, contentType); // Send it to the client
    file.close(); // Close the file again
    serialPrint(String("Sent file: ") + path);
    return true;
  }
  
  serialPrint(String("File Not Found: ") + path);
  return false; // If the file doesn't exist, return false
}

void WebServer::continuousLoop(bool triggerOneMinuteAction) {
  server.handleClient();
  if(triggerOneMinuteAction) {
    serialPrint("is up");
  }
}
