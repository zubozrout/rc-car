#ifndef RC_VEHICLE_H
#define RC_VEHICLE_H

#include <ESP8266WebServer.h>
#include <map>

class Vehicle {
  private:
    ESP8266WebServer *server;
    String webPath = "/control";
    bool usePost = true;
    struct pinsTemplate {
      int id;
      bool digital;
      String pinName;
      String actionName;

      pinsTemplate(int id, bool digital, String pinName, String actionName): id(id), digital(digital), pinName(pinName), actionName(actionName) {}
    };
    std::vector<pinsTemplate> pins;

    void init();
    void serialPrint(String message);
    
    void setPostAction();
    String getStatusInfo();
    void makeAction(String action = "", int stateValue = 0);
    int getPinIdByName(String pinName);
    void frontLights(bool turnOn);
    void redLights(bool turnOn);
    void runMotorAtSpeed(int speed);
    void runMotorDirection(bool direction);
    int getMotorSpeed();
  
  public:    
    Vehicle(ESP8266WebServer *server);
    Vehicle(ESP8266WebServer *server, String webPath);
    void continuousLoop(bool triggerOneMinuteAction = false);
};

#endif
