#ifndef OTA_H
#define OTA_H

#include <ArduinoOTA.h>

class OTA {
  private:
    String deviceHostname;
    String devicePassword;
    
    void arduinoOtaSetup();
    void serialPrint(String message);
    
  public:
    OTA(String deviceHostname = "esp", String devicePassword = "");
    void serialPrintStatus();
    void continuousLoop(bool triggerOneMinuteAction = false);
};

#endif
