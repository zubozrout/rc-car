#ifndef WIFI_H
#define WIFI_H

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>

class WiFiNetwork {
  private:
    WiFiManager wifiManager;
    String deviceHostname = "";
    
    void setupWiFi(bool enableDNS);
    void serialPrint(String message);
    
  public:
    WiFiNetwork(bool enableDNS = true, String deviceHostname = "esp");
    void serialPrintStatus();
    void continuousLoop(bool triggerOneMinuteAction = false);
};

#endif
