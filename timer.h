#ifndef TIMER_H
#define TIMER_H

#include "Arduino.h"

bool scheduleRepeatAction(unsigned long *previousMillis, int interval);

#endif
